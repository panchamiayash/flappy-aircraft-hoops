//
//  AppDelegate.h
//  proj4
//
//  Created by Yash Panchamia on 3/10/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

