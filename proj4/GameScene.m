//
//  GameScene.m
//  proj4
//
//  Created by Yash Panchamia on 3/10/17.
//  Copyright © 2017 Yash Panchamia. All rights reserved.
//

#import "GameScene.h"
#import "SKTUtils/SKTUtils.h"

@interface GameScene() <SKPhysicsContactDelegate>
{
  
}
@end

@implementation GameScene
{
    SKNode *bgLayer;
    SKNode *gameLayer;
    SKNode *HUDlayer;
    CGFloat dt;
    CGFloat lastUpdate;
    SKSpriteNode *player;
    SKTexture *pipe1;
    SKTexture *pipe2;
    BOOL gameOver;
    SKSpriteNode *gameOverMenu;
    int gameScore;
 
    SKSpriteNode *finalScore;
}

static const uint32_t player_category = 1<<0;
static const uint32_t platform_category = 1<<1;
static const uint32_t ring_category = 1<<2;
static const uint32_t world_category = 1<<3;
static const uint32_t score_category = 1<<4;
static const uint32_t pole_category = 1<<5;

static const int RINGS_MOVE_PER_SEC = 150;

- (void)didMoveToView:(SKView *)view
{
    for(SKNode *node in self.children)
    {
        [node removeFromParent];
    }
    self.physicsWorld.contactDelegate = self;
    
    self.physicsWorld.gravity = CGVectorMake(0.0f, -5.0f);
    
    bgLayer = [SKNode node];
    [self addChild:bgLayer];
    gameLayer = [SKNode node];
    [self addChild:gameLayer];
    HUDlayer = [SKNode node];
    [self addChild:HUDlayer];
    
    //GAME OVER MENU
    gameOverMenu = [SKSpriteNode spriteNodeWithImageNamed:@"GameOverImage"];
    gameOverMenu.hidden = YES;
    gameOverMenu.position = CGPointMake(0,0);
    [HUDlayer addChild:gameOverMenu];
    
    //move BGLayer
    SKTexture *bgTexture = [SKTexture textureWithImageNamed:@"backgroundI4"];
    SKAction *moveBg = [SKAction moveByX:-bgTexture.size.width*2 y:0 duration:0.1+bgTexture.size.width];
    SKAction *resetBG = [SKAction moveByX:bgTexture.size.width*2  y:0 duration:0];
    SKAction *moveBGForever = [SKAction repeatActionForever:[SKAction sequence:@[moveBg, resetBG]]];
    
    for(int i=0; i<2 + self.frame.size.width / (bgTexture.size.width *2); i++)
    {
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithTexture:bgTexture];
        [sprite setScale:2.0];
        sprite.zPosition = -20;
        //sprite.anchorPoint = CGPointZero;
        sprite.position =CGPointMake(i* sprite.size.width, 0);
        [sprite runAction:moveBGForever];
        [bgLayer addChild: sprite];
    }
    
    //move GroundLayer
    SKTexture *groundBg = [SKTexture textureWithImageNamed:@"Ground-1"];
    SKAction *moveGround = [SKAction moveByX:-groundBg.size.width*2 y:0 duration:0.02+groundBg.size.width];
    SKAction *resetGround = [SKAction moveByX:groundBg.size.width*2  y:0 duration:0];
    SKAction *moveGroundForever = [SKAction repeatActionForever:[SKAction sequence:@[moveGround, resetGround]]];
    
    for(int i=0; i<2 + self.frame.size.width / (groundBg.size.width *2); i++)
    {
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithTexture:groundBg];
        [sprite setScale:2.0];
        sprite.zPosition = -20;
        //sprite.anchorPoint = CGPointZero;
        sprite.position =CGPointMake(i* sprite.size.width, -self.frame.size.height/2 +30);
        [sprite runAction:moveGroundForever];
        [gameLayer addChild: sprite];
    }
    
    //ground and ceiling
    SKNode *dummyGround = [SKNode node];
    dummyGround.position = CGPointMake(0, -self.frame.size.height/2 + 60);
    dummyGround.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, groundBg.size.height * 2)];
    dummyGround.physicsBody.dynamic = NO;
    dummyGround.physicsBody.categoryBitMask = world_category;
    [gameLayer addChild:dummyGround];
    
    SKNode *dummyCeiling = [SKNode node];
    dummyCeiling.position = CGPointMake(0, self.frame.size.height/2);
    dummyCeiling.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, 1)];
    dummyCeiling.physicsBody.dynamic = NO;
    dummyCeiling.physicsBody.categoryBitMask = world_category;
    [gameLayer addChild:dummyCeiling];
    
    //PLAYER
    player = [SKSpriteNode spriteNodeWithImageNamed:@"pixelPlane"];
    player.position = CGPointMake(-100, 400);
    [player setScale:1.1];
    player.zPosition= 50;
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(46, 18)];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = NO;
    player.physicsBody.categoryBitMask = player_category;
    player.physicsBody.collisionBitMask = platform_category | ring_category | world_category |pole_category;
    player.physicsBody.contactTestBitMask = platform_category | ring_category | world_category |pole_category;
    [gameLayer addChild:player];
    
   
    
    //PIPES
    SKAction *spawn = [SKAction performSelector:@selector(spawnRings) onTarget:self];
    SKAction *delay = [SKAction waitForDuration:2.0];
    SKAction *spawnThenDelay = [SKAction sequence:@[spawn,delay]];
    SKAction *spawnThenDelayForever = [SKAction repeatActionForever:spawnThenDelay];
    [self runAction:spawnThenDelayForever];
    
    
    //SCORE
    gameScore = 0;
    finalScore = [SKSpriteNode node];
    NSString *digit = [NSString stringWithFormat:@"%i",gameScore];
    [self setScoreSprite:(int)digit.length];
    finalScore.zPosition = 201;
    [HUDlayer addChild:finalScore];
    
    
    
}//end


-(void)spawnRings
{
    SKSpriteNode *behindring = [SKSpriteNode spriteNodeWithImageNamed:@"PipeBehindHalf"];
    behindring.zPosition = 20;
    [behindring setScale:1.6];
    SKSpriteNode *frontRing = [SKSpriteNode spriteNodeWithImageNamed:@"PipeInFront"];
    frontRing.zPosition = 100;
    [frontRing setScale:1.6];
    
    pipe1 = [SKTexture textureWithImageNamed:@"pipe"];
    pipe1.filteringMode = SKTextureFilteringNearest;
    pipe2 = [SKTexture textureWithImageNamed:@"pipe"];
    pipe2.filteringMode = SKTextureFilteringNearest;
    
    SKSpriteNode *pole = [SKSpriteNode spriteNodeWithTexture:pipe1];
    [pole setScale:1.5];
    pole.hidden =NO;
    pole.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:pole.size];
    pole.physicsBody.dynamic = NO;
    
    SKSpriteNode *poleTop = [SKSpriteNode spriteNodeWithTexture:pipe2];
    [poleTop setScale:1.5];
    poleTop.hidden =NO;
    poleTop.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:poleTop.size];
    poleTop.physicsBody.dynamic = NO;
    
    //make and move rings
    float changeFactor = self.frame.size.height/2-100;
    CGFloat y = arc4random() % (NSInteger)(changeFactor);
//    pole.position = CGPointMake(self.size.width +150, y-515);
//    poleTop.position = CGPointMake(self.size.width +150, y-350 + pole.size.height +128-145);
//    behindring.position = CGPointMake(self.size.width +120, y-450 + pole.size.height -106-400);
//    frontRing.position = CGPointMake(self.size.width +180, y-450 + pole.size.height -106-400);
    pole.position = CGPointMake(self.size.width/4 +150, y-515);
    poleTop.position = CGPointMake(self.size.width/4 +150, y-350 + pole.size.height +128-145);
    behindring.position = CGPointMake(self.size.width/4 +120, y-450 + pole.size.height -106-400);
    frontRing.position = CGPointMake(self.size.width/4 +180, y-450 + pole.size.height -106-400);

    frontRing.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:frontRing.size];
    frontRing.physicsBody.dynamic = NO;
    frontRing.physicsBody.categoryBitMask = score_category;
    frontRing.physicsBody.contactTestBitMask = player_category;
    
    pole.physicsBody.categoryBitMask = pole_category;
    pole.physicsBody.contactTestBitMask = player_category;
    
    poleTop.physicsBody.categoryBitMask = pole_category;
    poleTop.physicsBody.contactTestBitMask = player_category;
    
    pole.name = @"pole";
    poleTop.name = @"pole";
    behindring.name = @"pole";
    frontRing.name = @"pole";
    
    [gameLayer addChild:pole];
    [gameLayer addChild:poleTop];
    [gameLayer addChild:behindring];
    [gameLayer addChild:frontRing];
    
}

-(void)moveRings
{
    [gameLayer enumerateChildNodesWithName:@"pole" usingBlock:^(SKNode *node, BOOL *stop)
     {
         SKSpriteNode *sprite = (SKSpriteNode*) node;
         if(sprite.position.x < -self.size.width/2 - 20)
         {
             [sprite removeFromParent];
         }
         else
         {
             CGPoint distanceToMove = CGPointMake(-RINGS_MOVE_PER_SEC, 0);
             CGPoint amtToMove = CGPointMultiplyScalar( distanceToMove, dt);
             sprite.position = CGPointAdd(amtToMove, sprite.position);
         }
     }];
}

- (void)touchDownAtPoint:(CGPoint)pos
{
}

- (void)touchMovedToPoint:(CGPoint)pos
{
}

- (void)touchUpAtPoint:(CGPoint)pos
{
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *myTouch = [[touches allObjects] objectAtIndex:0];
    CGPoint currPos = [myTouch locationInNode:self];
    if(gameLayer.speed > 0)
    {
        player.physicsBody.velocity = CGVectorMake(0, 0);
        [player.physicsBody applyImpulse:CGVectorMake(0, 14)];
    }
    if(gameOver)
    {
        if(currPos.x >= 0-90 && currPos.x <= 0+90 && currPos.y >= 0-100 && currPos.y <= 0-50)
        {
            SKScene *myscene = [[GameScene alloc] initWithSize:self.size];
            SKTransition *reveal =[SKTransition doorwayWithDuration:0.5];
            reveal.pausesIncomingScene=NO;
            [self.view presentScene:myscene transition:reveal];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)even
{
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    if(gameLayer.speed > 0)
    {
        if((contact.bodyA.categoryBitMask & score_category) == score_category || (contact.bodyB.categoryBitMask & score_category) == score_category)
        {
            gameScore++;
            NSMutableString *scoreString = [NSMutableString stringWithFormat:@"%i",gameScore];
            int length =(int)scoreString.length;
            [self setScoreSprite: length];
        }
        else if ((contact.bodyA.categoryBitMask & world_category) == world_category || (contact.bodyB.categoryBitMask & world_category) == world_category)
        {
            
        }
        else
        {
            gameLayer.speed = 0;
            bgLayer.speed = 0;
            [self removeAllActions];
            player.physicsBody.collisionBitMask = world_category;
            [player runAction:[SKAction rotateByAngle:M_PI * player.position.y *0.01 duration:player.position.y *0.003] completion:^{
                player.speed = 0;
            }];
            
            gameOverMenu.hidden =NO;
            gameOverMenu.zPosition = 500;
            NSMutableString *scoreString = [NSMutableString stringWithFormat:@"%i", gameScore];
            int length  =(int) scoreString.length;
            finalScore.zPosition = 501;
            gameOver = YES;
            [self setScoreSprite:length];
        }
    }
}

-(void)setScoreSprite:(int)numberOfDigitInScore
{
    NSMutableString *scoreString = [NSMutableString stringWithFormat:@"%i", gameScore];
    [finalScore removeAllActions];
    [finalScore removeAllChildren];
    double var;
    int sides;
    CGFloat y = -17;
    if(gameOver == NO)
    {
        y=self.size.height/2 - 50;
    }
    switch (numberOfDigitInScore) {
        case 1:
        {
            NSString *digit = [scoreString substringFromIndex:0];
            SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:[NSString stringWithFormat:@"Number%@",digit]];
            sprite.position = CGPointMake(0, y);
            [finalScore addChild:sprite];
            break;
        }
            
        case 2:
        {
            for(int i=0; i<scoreString.length; i++)
            {
                sides = (i!=0) ? 1 : -1;
                NSString *digit = [scoreString substringWithRange:NSMakeRange(i, 1)];
                SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:[NSString stringWithFormat:@"Number%i",digit.intValue]];
                if(digit.intValue == 1)
                {
                    var = 5;
                }
                else
                {
                    var = 0;
                }
                sprite.position = CGPointMake(0 +(15 - var)*sides, y);
                [finalScore addChild:sprite];
            }
            break;
        }
        default:
            break;
    }//end switch
}


-(void)update:(CFTimeInterval)currentTime
{
    CGFloat currTime = currentTime;
    if(lastUpdate)
    {
        dt = currTime - lastUpdate;
    }
    else
    {
        dt = 0;
    }
    player.zRotation = clamp(-.5, 0.5, player.physicsBody.velocity.dy * (player.physicsBody.velocity.dy < 0 ? 0.001 : 0.001));
    
    lastUpdate = currentTime;
    if(gameOver == NO)
    {
        [self moveRings];
    }
}

CGFloat clamp(CGFloat min, CGFloat max, CGFloat value)
{
    if(value >max)
    {
        return max;
    }
    else if(value < min)
        {
            return min;
        }
    else
        {
            return value;
        }
}

@end
